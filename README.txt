DeviceAtlas Module
----------------------
by Lawrence Miller, ajacs1@gmail.com

Description
-----------

The DeviceAtlas module integrates the commercial DeviceAtlas mobile device
capability database with Drupal. Customers of the DeviceAtlas Cloud API service
just need to enter their API key in the module configuration in order to begin
using DeviceAtlas data within their Drupal sites.

What's included
---------------

The DeviceAtlas package consists of three modules:

 - DeviceAtlas API: enable this module and enter your API key on the
   configuration menu, and a new global variable containing all the DeviceAtlas
   information becomes available. A Theme variable is also added.

 - DeviceAtlas Info (depends on devel): Any user with the administer DeviceAtlas
   permission may visit /deviceatlas/info to see all the info DeviceAtlas has
   captured about their current device.

 - DeviceAtlas Redirect: redirects mobile visitors to a URL of your choice based
   on the width of the visitor's screen.

Configuration & Usage
---------------------

 - Visit /admin/settings/deviceatlas to enter your API key and caching 
   preferences.  Cookie caching is strongly recommended.

 - You can access a variable called $_deviceatlas_data from your own php code.
   See the deviceatlas_info/deviceatlas_info.module file for an example how to 
   access the variable.  This variable contains all the information DeviceAtlas
   can determine about the visitor's device.

 - The information in $_deviceatlas_data is also available to your theme tpl.php
   files via $variables['deviceatlas'].  

TODO:
-----

DeviceAtlas API: integration with Drupal buit in caching
DeviceAtlas Redirect: redirect based on additional capabilities